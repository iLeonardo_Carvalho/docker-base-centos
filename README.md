![CentOS Logo](https://lh3.googleusercontent.com/MOwbmnDpggJvzHMOv0PwIVL41rbTbdW5ylb8NBdfdY1Q3VC29buM7WOCYh2Pqr1XFLBvETqvoSUbLimf-s0a962_WBU9BRFJMbZKEYGj4IgXo_JGkpU8V1TKbiXWN361PTdNrcVLI3znaTE6HIYlAT6nChqOcJni8IjtWERUoP8HOi1VSu24OxgqmNGy_6l8_gcAUl5bEVqOiVZeX0955avbqzcL9xOxAFLzJshYrp01_hZM7W70lcqvdq6jmeaWppz1UTyse5f0iaw8JFq0Y0Db6PWDEyQn6JFuaflfldvPcG3HVfPcsEuhADV46z3wGg2kz7U1eC2iBncODYhQRM8Ydry-Zvr8QpU5jAk_XinO6fWblkASDLP4Pko7H9M-6vTooYX_cy6GbmVcNBwTQ463mGJgBDBfXOmx8mBt96sC0JGThyC4U2tpKUiWbs1_1vfowg5H77XK82XYOVPxdT1osUM_wfcXOeG9ptvSLmUBhVMEQMIgUUYp5qSSpqMlPxjGj6pvEQTZypNuMKBaCjVDNUga9ohwcLUP1Hz3KR7-AQTcCJ5l9hrelLbDYwWkJP_4EfDJrRMhQWEyiLD5Wdc9xTExslsF25vGrciTAg=w449-h673-no)




Docker CentOS/Base
===================

Este container contém é uma solução do **CentOS:7** sobre o erro:

> Failed to get D-Bus connection: No connection to service manager.

Quando o **systemctl** não tem privilégios no Kernel.

------



[TOC]

## Requisitos

Foi utilizado a versão **Release e Oficial** do próprio [Docker Hub](https://hub.docker.com/_/centos/) no dia **21 de Novembro de 2016** e testado usando o Docker Engine e Docker Kitematic.

> **Notas:**

> - [systemd](#systemd)
> - [epel-release](#epel-release)
> - [net-tools](#net-tools)
> - [iputils](#iputils)
> - [vim](#vim)
> - [curl](#curl)

> **Obs.:**

> - LATEST (últimas atualizações de acordo com o acesso)
> - É recomendado ao utilizar em outros serviços colocando os privilégios.

#### Funcionamento

**Docker Engine**
```
docker run -it -e "container=docker" --privileged=true -d --security-opt seccomp:unconfined --cap-add=SYS_ADMIN -v /sys/fs/cgroup:/sys/fs/cgroup:ro ileonardo/base-centos bash -c "/usr/sbin/init"
```
**Docker Compose**
```yaml
base-centos:
  build: centos/
  privileged: true
  cap_add:
      - SYS_ADMIN
  security_opt:
      - seccomp:unconfined
  volumes:
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
```
**Docker Kitematic**

![Docker Kitematic Privileged Mode](https://lh3.googleusercontent.com/F_OlUHv4c6ZXxMaptRYcbd3nHyq0i7_pm-DedD45lHjF_AK908BRfMxILZujt38pa9fbhl-pxZNm_pKogZvq7si7AUzBHH5ptWh7gRm8nnw6cNPRSaCuo5NevkJ_lDwnbRzucS_-L-Qd92WZC3m_UfFnuAT77EskZX0gqB-Uumdc-s-4qaiphEk_2DvIufD72OZ9Dj75XBySUcODm6F_ZCQgjpxD1xMqsrtU5blLExoe1iOK5yG5aaGm5VNAPMbZZ0eIgFq5_yF_5oDAQaMSR-KE95L8MUZXWMIo3LLkMEQQ0zRSdkfOz-ndIdZUfNDMO90mgJMmk0EBuVCO5L97v5Qhi3otThKbPSnpSs6fR5qPMl1Ygx_btBaXUiujfDVjMgnpqC1mfLjggHc3zsiUKSdZgwQHZ3fVmGmDd7X6oTKqxLKJYUAK_JKScqbXtf_0Ax6Bx6RzKZoaLQ-KaP1Qjwl_vFJRFs3ZdSt8pGokufWiXBmX8oFgFeKRL7oZI-9iSYNiD0JRnRQuLln75a1g8j5FFSrR0qaG-zzdJqBgVOFrDW0zaEBALBXKpDDTbDNgpCAvXfYay9B6_4aF7ClJeb2anEJv1RxLpRAqpIMMaw=w1198-h673-no)

![Docker Kitematic Running](https://lh3.googleusercontent.com/T4JyA6gou9IG3UUCyzI5UiZNCAGQgR5KkHav7gsqaJwn1Y9WEPcga7LObwGdX-G034-T6GEN3MrOPbsONNNpGEFyctavPX7q70K0o6KHU7iT0HhUo3qoMX0P-UGkcR1R4lNfIDUyTaFFbLvztm9HEYTKVh_SxLduqjMl8gnuL1McQoqTdfx3TgHxVmx-Wyq0bOEm_Zvrc7NFLQnwg0UVkb07wPf3lZ2o6Ej4CE-fXT7_eEi-CWUzDYefxr2u0mlFCQo85G4yKgHBbie-MMUZxQ8oizHoV3VltgH8kE3AXVc1f0Zz-C29pGgVHaA00v9OxA6ka2nyusjCvHORmd3PVdvZS3JJJH8a9whhuUNHXZV3Xqd0qWYTN22Ok28dRuEpSheoOI-Weo430PK7iqZSFq0wZHVAiA1Lr0XhytfoytEQ12-P0K60RlOFGYCjjRKQT75lJ048M5PeKeKTi18rqphjz65IBFr_94OygtmI0VPDIJLBvVf08pJWW76Id83FX255wYZ_2DHOBdDWQHNZy1U-zDscurhroHwXLW0Ii5u-q_POe7Dr98Sx_nQOu4nb4DOLyF4pKAqTNy7KSfkdonX5duZTUwvrTwOqu6EtLA=w1198-h673-no)

> **Obs.:** Verifique também se todas às configurações estão corretamente nas outras abas.

----------


Comandos Básicos
-------------------

#### Uso

Utilize:

Para construir a partir do Código Fonte:

```
docker build -t ileonardo/base-centos .
```

> **Obs:** O ponto final significa que o **Dockerfile** esta na pasta atual.

Ou se preferir baixar pelo [Docker Hub](https://hub.docker.com/explore/):

```
docker pull ileonardo/elk
```

Entrar no Container em daemon:

```
docker exec -it <ID_Container> /bin/bash
```

Sair sem encerrar o Container:

```
[CTRL] p + q
```

#### Essenciais

Ver todas as Imagens no **HOST**:

```
docker images
```

Ver todos os Containers no **HOST**:

```
docker ps -a
```

Remover uma Imagem no **HOST**:

```
docker rmi <nome_imagem>
```

Remover um Container no **HOST**:

```
docker rm <Nome_Container>
```

Remover *dangling images* (Imagens sem TAG, quer dizer quando rodou o **Dockerfile** que falhou ele cria uma imagem <none>)

```
docker rmi -f $(docker images | grep "<none>" | awk "{print \$3}")
```

Remover o histórico dos comandos do Container no **HOST**:

```
docker stop $(docker ps -a -q) && docker rm $(docker ps -a -q)
```

Remover Todas às Imagens e Containers no **HOST**:

```
docker stop $(docker ps -a -q) && \
docker rm $(docker ps -a -q) && \
docker rmi $(docker images -q)
```

#### Manipulação de Dados

Copiar um arquivo do Container para o **HOST**:

```
docker cp <ID_Container>:/caminho/no/container/arquivo /caminho/no/host
```

Copiar um arquivo do **HOST** para o Container:

```
docker cp /caminho/no/host/arquivo <ID_Container>:/caminho/no/container
```

#### Monitoramento de Containers

Para ver as estatísticas de um Container específico no **HOST**:

```
docker stats <Nome_container>
```

Para ver as estatísticas de **todos** Containers no **HOST**:

```
docker stats `docker ps | tail -n+2 | awk '{ print $NF }'`
```


Direitos autorais e Licença
-------------

Este trabalho não foi modificado de seus Criadores (Link's de consulta abaixo), foi adaptado de acordo com a documentação do mesmo e dando os créditos contida neste repositório, a busca e a organização para futuras atualizações deve ser dado ao **contributors.txt** (BY).

Este trabalho foi escrito por Leonardo Cavalcante Carvalho e está licenciado com uma [Licença **Apache-2.0**](https://www.apache.org/licenses/LICENSE-2.0).

[^stackedit]: [StackEdit](https://stackedit.io/) is a full-featured, open-source Markdown editor based on PageDown, the Markdown library used by Stack Overflow and the other Stack Exchange sites.
[^docker]: A instalação foi utilizado no domínio do [Docker](https://get.docker.com/) que está contido o script auto installer.
[^ELK]: A instalação Source das dependências foram baseadas no site [Mitesh Shah](https://miteshshah.github.io/linux/elk/).
[^Mundo Docker]: Configuração e a descrição do comando documentada baseado nos tutoriais do [Mundo Docker](www.mundodocker.com.br).